library(readr)
library(dplyr)

# read and view NYC data
nyc_311 <- read_csv("data/311_Service_Requests_from_2010_to_Present_nyc.csv")
View(nyc_311)

# identify columns to keep
keeps <- c("Unique Key", "Created Date", "Closed Date", "Descriptor", "Status", "Borough")
nyc_update <- nyc_311[keeps]
View(nyc_update)

# read and view Oakland data
oakland_311 <- read_csv("data/311_Oakland_20200301-present.csv")
oak02 <- oakland_311
oak02 <- select (oakland_311,-c(REQCATEGORY, State))
View(oak02)

# Normalize variable names, matching oakland to nyc headers
colnames(oak02)[colnames(oak02) == "REQUESTID"] <- "Unique Key"
colnames(oak02)[colnames(oak02) == "DATETIMEINIT"] <- "Created Date"
colnames(oak02)[colnames(oak02) == "DESCRIPTION"] <- "Descriptor"
colnames(oak02)[colnames(oak02) == "STATUS"] <- "Status"
colnames(oak02)[colnames(oak02) == "DATETIMECLOSED"] <- "Date Closed"
colnames(oak02)[colnames(oak02) == "City"] <- "Location"
View(oak02)

colnames(nyc_update)[colnames(nyc_update) == "Borough"] <- "Location"
View(nyc_update)

# combine nyc and oakland data with vertical integration
all_city_data <- bind_rows(nyc_update, oak02)

View(all_city_data)

# export to csv
write.csv(all_city_data,"data\\ex5_integration_311Data_SNguyen.csv", row.names = FALSE)
